const express = require("express");
const app = express();
const sql = require("./db");
const port = 3000;

app.get("/", (req, res) => {
  sql.query("Select * from country_and_capitals", function (err, results) {
    if (err) {
      res.send(err);
    } else {
      res.status(200).send(results);
    }
  });
});

app.listen(port, () => console.log(`Example app listening on port 3000!`));
